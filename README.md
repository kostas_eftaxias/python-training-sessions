# Prerequisites for Python Training Sessions #

Welcome to the Python Training Sessions! During the next weeks we will learn how to use Python to automate some everyday
tasks. We will mainly focus on opening, editing and saving Excel and csv files using Python scripts. Moreover, we will cover
spatial files and basic GUI creation using PyQT. 

The first sessions will be focused on Python basic syntax and data structures. In order to save some
time on the first session it is required to set up your laptops beforehand. A more detailed schedule and instructions 
for what is needed for the first session can be found below.  

### Training Schedule (STC) ###
| Session #   |      Subject      |  Date |
|:----------:|:-------------:|:------:|
| 1 | Introduction to Python and Tools, Hello World example, Basic Functions, Flow Control Statements   | April 2019 (across different offices) |
| 2 | Python Data Structures | TBD (Skype Session) | 
| 3 | CSV-Excel file manipulation | TBD (Skype Session) |
| 4 | GUI creation | TBD (Skype Session) |
| 5 | Spatial Data | TBD (Skype Session) |


### How do I set up my laptop before the first session? ###

In this tutorial series, we will use the open source Anaconda distribution which comes 
with a friendly user and all the tools that will be needed. A sophisticated
IDE is also suggested and for this series we are going to use the basic functionality of Pycharm. 

No need for admin rights for both installations and a disk space of approximately 5 GBs is needed. 

#### Anaconda ####

* Download  Anaconda installer (Python 3.7 version) from this [link](https://repo.anaconda.com/archive/Anaconda3-2018.12-Windows-x86_64.exe)
* Double-click the .exe file
* Follow the instructions on the screen 
* Select "just me" to avoid the request for admin rights

#### Pycharm

* Download  Pycharm  installer (Community Edition) from this [link](https://www.jetbrains.com/pycharm/download/download-thanks.html?platform=windows&code=PCC)
* Double-click the .exe file
* Select No on the UAC dialogue that comes up
* Follow the instructions on the screen 


