# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'SATURN to GIS Conversion -main.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import pyqtSlot
import os
import pandas as pd
import time

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(818, 480)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("wsp_cmyk-01.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(710, 410, 91, 41))
        self.label.setText("")
        self.label.setPixmap(QtGui.QPixmap("wsp_cmyk-01.png"))
        self.label.setScaledContents(True)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(250, 40, 324, 29))
        font = QtGui.QFont()
        font.setPointSize(18)
        font.setBold(True)
        font.setWeight(75)
        self.label_2.setFont(font)
        self.label_2.setScaledContents(False)
        self.label_2.setObjectName("label_2")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(310, 170, 471, 20))
        self.label_5.setFrameShape(QtWidgets.QFrame.Box)
        self.label_5.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setGeometry(QtCore.QRect(310, 220, 471, 20))
        self.label_6.setFrameShape(QtWidgets.QFrame.Box)
        self.label_6.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.label_6.setObjectName("label_6")
        self.layoutWidget_2 = QtWidgets.QWidget(self.centralwidget)
        self.layoutWidget_2.setGeometry(QtCore.QRect(20, 220, 250, 24))
        self.layoutWidget_2.setObjectName("layoutWidget_2")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.layoutWidget_2)
        self.gridLayout_3.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.label_7 = QtWidgets.QLabel(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_7.setFont(font)
        self.label_7.setObjectName("label_7")
        self.gridLayout_3.addWidget(self.label_7, 0, 0, 1, 1)
        self.toolButton_3 = QtWidgets.QToolButton(self.layoutWidget_2)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("Flat Folder icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.toolButton_3.setIcon(icon1)
        self.toolButton_3.setCheckable(False)
        self.toolButton_3.setObjectName("toolButton_3")
        self.gridLayout_3.addWidget(self.toolButton_3, 0, 1, 1, 1)
        self.layoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.layoutWidget.setGeometry(QtCore.QRect(130, 170, 134, 24))
        self.layoutWidget.setObjectName("layoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.layoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.toolButton = QtWidgets.QToolButton(self.layoutWidget)
        self.toolButton.setIcon(icon1)
        self.toolButton.setCheckable(False)
        self.toolButton.setObjectName("toolButton")
        self.gridLayout.addWidget(self.toolButton, 0, 1, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.layoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 0, 0, 1, 1)
        self.layoutWidget1 = QtWidgets.QWidget(self.centralwidget)
        self.layoutWidget1.setGeometry(QtCore.QRect(300, 370, 241, 51))
        self.layoutWidget1.setObjectName("layoutWidget1")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.layoutWidget1)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setSpacing(43)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.pushButton = QtWidgets.QPushButton(self.layoutWidget1)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton.sizePolicy().hasHeightForWidth())
        self.pushButton.setSizePolicy(sizePolicy)
        self.pushButton.setObjectName("pushButton")
        self.horizontalLayout.addWidget(self.pushButton)
        self.pushButton_2 = QtWidgets.QPushButton(self.layoutWidget1)
        self.pushButton_2.setObjectName("pushButton_2")
        self.horizontalLayout.addWidget(self.pushButton_2)
        self.label_8 = QtWidgets.QLabel(self.centralwidget)
        self.label_8.setGeometry(QtCore.QRect(20, 260, 451, 22))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_8.setFont(font)
        self.label_8.setObjectName("label_8")
        self.comboBox = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox.setGeometry(QtCore.QRect(470, 260, 69, 22))
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.layoutWidget.raise_()
        self.layoutWidget.raise_()
        self.label.raise_()
        self.label_2.raise_()
        self.label_5.raise_()
        self.label_6.raise_()
        self.layoutWidget_2.raise_()
        self.label_8.raise_()
        self.comboBox.raise_()
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        @pyqtSlot()
        def openFolder():
            global XEXESpath
            XEXESpath = QtWidgets.QFileDialog.getExistingDirectory(MainWindow, 'Set XEXES folder')
            if XEXESpath:
                print(XEXESpath)
                self.label_5.setText(XEXESpath)
                return XEXESpath

        @pyqtSlot()
        def openFile():
            global filename, filepath
            filename = QtWidgets.QFileDialog.getOpenFileName(MainWindow, 'Location of Network file (UFN or UFS):',
                                                             os.getcwd(), " *.UFN, *.UFS")
            if filename:
                print(filename[0])
                self.label_6.setText(filename[0])
                filepath = filename[0].split("/")[:-1]
                filename = filename[0].split("/")[-1]
                filepath = '/'.join(filepath) + '/'
                print(filepath)
                return filename, filepath

        @pyqtSlot()
        def exitprogram():
            MainWindow.close()



        @pyqtSlot()
        def runscript():
            with open(filepath+'GIS.key', 'w') as f:
                f.write("       947       267   66    0  SATDB Opts     (Mouse pixels/status/Line)\n")
                f.write("2\n")
                f.write("6\n")
                f.write("2\n")  #remove simulation turns
                f.write("3\n")  #remove simulation centroids
                f.write("5\n")  #remove buffer centroids
                f.write("0\n")
                f.write("0\n")
                f.write("6\n")  #add co-ordinates
                f.write("6\n")
                f.write("0\n")
                f.write("4\n")
                f.write("1\n")
                f.write("1\n")  #1 - lanes
                f.write("5\n")  #2 - FF speed
                f.write("6\n")  #3 - Distance
                f.write("9\n")   #4 - Average Q
                f.write("11\n") #5 - Capacity
                f.write("12\n") #6 - V/C
                f.write("14\n") #7 - Delay
                f.write("17\n") #8 - Total Time
                f.write("18\n") #9 - Net Speed
                f.write("24\n") #10 - Actual Flow
                f.write("201\n") #10 - Loop over all
                f.write("0\n") #all UCs
                f.write("41\n") #Actual Flow looped
                f.write("0\n")
                f.write("13\n")
                f.write("0\n")
                f.write("GISout.txt\n")
                f.write("0\n")
                f.write("       917       439         0 55510.3 25051.8 (Mouse pixels/status/X,Y)\n")
                f.write("       914       445   81    0  Quit = Exit    (Mouse pixels/status/Line)\n")
                f.write("Yes OK to exit the program?\n")
                f.close()

            with open(filepath + 'p1x_GIS.bat', 'w') as f:
                f.write('Path = C:\WINDOWS\System32;C:\WINDOWS\System;C:\WINDOWS\Command;C:\WINDOWS;' + '"'+XEXESpath.replace('/',os.path.sep)+os.path.sep+'";""\n')
                f.write("SET QUIET=True\n")
                f.write("SET QUICK=False\n")
                f.write("SET FTN95_NEW_MEMORY=True\n")
                f.write(filepath[:2]+'\n')
                f.write('CD "' +filepath.replace('/',os.path.sep)+'"\n')
                f.write("call p1x " + filename[:-4] + " KEY GIS VDU NUL\n")
                f.close

            os.startfile(filepath + 'p1x_GIS.bat')
            time.sleep(5)

            if os.path.exists(filepath + filename[:-4]+ '.lpn'):
                if 'DUTCH  =    F' in open(filepath + filename[:-4]+ '.lpn').read():
                    isDUTCH = False
                elif 'DUTCH  =    T' in open(filepath + filename[:-4] + '.lpn').read():
                    isDUTCH = True
            elif os.path.exists(filepath + filename[:-4]+ '.lpt'):
                if 'DUTCH  =    F' in open(filepath + filename[:-4]+ '.lpt').read():
                    isDUTCH = False
                elif 'DUTCH  =    T' in open(filepath + filename[:-4] + '.lpt').read():
                    isDUTCH = True
            elif os.path.exists(filepath + filename[:-4] + '.dat'):
                if 'DUTCH  =    F' in open(filepath + filename[:-4]+ '.dat').read():
                    isDUTCH = False
                elif 'DUTCH  =    T' in open(filepath + filename[:-4] + '.dat').read():
                    isDUTCH = True
            else:
                isDUTCH = (self.comboBox.currentText()=='True')
            print(isDUTCH)


            if isDUTCH == False:
                gisout = pd.read_fwf(filepath + 'GISout.txt', header=None,
                                 widths=[5, 5, 18, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,
                                         10, 10, 10, 10, 10, 10],
                                 names=['A_Node', 'B_Node', 'A_Node_X', 'A_Node_Y', 'B_Node_X', 'B_Node_Y', 'Lanes',
                                        'FF_Speed', 'Distance',
                                        'Average_Q', 'Capacity', 'V_C', 'Delay', 'Total_Time', 'Net_Speed',
                                        'Actual_Flow', 'Actual_Flow_UC1',
                                        'Actual_Flow_UC2', 'Actual_Flow_UC3', 'Actual_Flow_UC4', 'Actual_Flow_UC5',
                                        'Actual_Flow_UC6',
                                        'Actual_Flow_UC7', 'Actual_Flow_UC8', 'Actual_Flow_UC9', 'Actual_Flow_UC10'])
            elif isDUTCH ==True:
                gisout = pd.read_fwf(filepath + 'GISout.txt', header=None,
                                 widths=[10, 10, 20, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,
                                         10, 10, 10, 10, 10, 10],
                                 names=['A_Node', 'B_Node', 'A_Node_X', 'A_Node_Y', 'B_Node_X', 'B_Node_Y', 'Lanes',
                                        'FF_Speed', 'Distance',
                                        'Average_Q', 'Capacity', 'V_C', 'Delay', 'Total_Time', 'Net_Speed',
                                        'Actual_Flow', 'Actual_Flow_UC1',
                                        'Actual_Flow_UC2', 'Actual_Flow_UC3', 'Actual_Flow_UC4', 'Actual_Flow_UC5',
                                        'Actual_Flow_UC6',
                                        'Actual_Flow_UC7', 'Actual_Flow_UC8', 'Actual_Flow_UC9', 'Actual_Flow_UC10'])


            gisout['B_Node'] = gisout['B_Node'].astype(int)
            gisout.insert(0, 'Reference', gisout['A_Node'].astype(str)+'-'+gisout['B_Node'].astype(str))


            gisout['A_Node_X'] = gisout['A_Node_X'].astype(int)
            gisout['A_Node_Y'] = gisout['A_Node_Y'].astype(int)
            gisout['B_Node_X'] = gisout['B_Node_X'].astype(int)
            gisout['B_Node_Y'] = gisout['B_Node_Y'].astype(int)

            x = gisout['A_Node_X']
            gisout['A_Node_X'] = gisout['A_Node_X'].apply(lambda x: x * 10 if len(str(x)) == 5 else x)
            x = gisout['A_Node_Y']
            gisout['A_Node_Y'] = gisout['A_Node_Y'].apply(lambda x: x * 10 if len(str(x)) == 5 else x)
            x = gisout['B_Node_X']
            gisout['B_Node_X'] = gisout['B_Node_X'].apply(lambda x: x * 10 if len(str(x)) == 5 else x)
            x = gisout['B_Node_Y']
            gisout['B_Node_Y'] = gisout['B_Node_Y'].apply(lambda x: x * 10 if len(str(x)) == 5 else x)

            gisout.to_csv(filepath + filename[:-4] + '_GIS.csv', index=False)



        self.toolButton.clicked.connect(openFolder)
        self.toolButton_3.clicked.connect(openFile)
        self.pushButton_2.clicked.connect(exitprogram)
        self.pushButton.clicked.connect(runscript)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "SATURN to GIS Conversion"))
        self.label_2.setText(_translate("MainWindow", "SATURN to GIS Conversion"))
        self.label_5.setText(_translate("MainWindow", "XEXES Folder Path"))
        self.label_6.setText(_translate("MainWindow", "Network File Path"))
        self.label_7.setText(_translate("MainWindow", "Location of Network file (UFN or UFS):"))
        self.toolButton_3.setText(_translate("MainWindow", "..."))
        self.toolButton.setText(_translate("MainWindow", "..."))
        self.label_3.setText(_translate("MainWindow", "Set XEXES folder:"))
        self.pushButton.setText(_translate("MainWindow", "Run"))
        self.pushButton_2.setText(_translate("MainWindow", "Exit"))
        self.label_8.setText(_translate("MainWindow", "If LPN, LPT and DAT file are not available, please provide "
                                                      "the value of DUTCH:"))
        self.comboBox.setItemText(0, _translate("MainWindow", "True"))
        self.comboBox.setItemText(1, _translate("MainWindow", "False"))
